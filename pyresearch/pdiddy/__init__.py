from collections import OrderedDict
from pyresearch.pdiddy.sharedfunctions import *
import pandas as pd
import pyresearch



docs_dict = OrderedDict()

#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }

docs_dict['pdiddy_iterable'] = {'fargs':'in_list: a list of some items to run against a pdiddy query\nquery: the pdiddy query you wish to run in string format (do not include the leading pdiddy term with this query\nopt: any options you wish to include with the query in string format',
               'fret': 'output_df: A dataframe that has the results from pdiddy for each item queried',
               'shortdesc': 'Iterate through a list, running pdiddy queries for each item, returns a dataframe with all results for each item queried',
               'desc': 'Iterate through a list, running pdiddy queries for each item, returns a dataframe with all results for each item queried\nUsage example: lst = ["document-sharepoint.com","mail.document-sharepoint.com","microsoft.ec","microcheckupdate.org"]\nnjalla_df = pdiddy_iterable(lst, "name ", " -t 2020/01/01")'
               }

def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)

