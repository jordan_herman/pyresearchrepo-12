
#
import pandas as pd
import pyresearch
import requests
import json
def pdiddy_iterable(in_list, query, opt):

    myipy = get_ipython()
    myipy.run_line_magic(u'pdiddy', 'set pdiddy_display_json False')
    myipy.run_line_magic(u'hostaxis', 'set pdiddy_display_df False')
    frames = []
    for x in in_list:
        tmp = None
        myipy.run_line_magic(u'pdiddy', query + x + opt)
        frames.append(myipy.user_ns['prev_pdiddy_df'])
    output_df = pd.concat(frames)
    myipy.run_line_magic(u'pdiddy', 'set pdiddy_display_df False')
    output_df = pyresearch.util.fixBeakerBool(output_df)
    output_df = output_df.reindex(axis=0)
    return output_df    